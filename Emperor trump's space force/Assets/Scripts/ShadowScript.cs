﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowScript : MonoBehaviour {

    GameObject objectAttachedTo;
	[SerializeField]
	Vector3 offsetToObject; 
	public bool canLand = true;
	private bool landActivated = false;
	[SerializeField]
	float timeToLand;
	private float timeToGround;
	private float shadowStepSizeX;
	private float shadowStepSizeY;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.E) == true && canLand == true)
		{
             landActivated = true;
			 timeToGround = timeToLand;
             shadowStepSizeX = (objectAttachedTo.transform.position.x - this.gameObject.transform.position.x) / timeToLand;
			 shadowStepSizeY = (objectAttachedTo.transform.position.y - this.gameObject.transform.position.y) / timeToLand;

		}
		if(objectAttachedTo != null)
		{
            transform.position = objectAttachedTo.transform.position + offsetToObject;
            transform.rotation = objectAttachedTo.transform.rotation;
		}
		if(landActivated == true)
		{
            Land(timeToGround);
		}
	}
	public void AttachShadow(GameObject ship)
	{
       objectAttachedTo = ship;
	}
	public void Land(float timeToGround)
	{
      timeToLand -= shadowStepSizeX;
	  this.transform.position += new Vector3(shadowStepSizeX, shadowStepSizeY,0);
	  offsetToObject = transform.localPosition;

      Debug.Log(new Vector3(shadowStepSizeX, shadowStepSizeY,0));
	}
}
