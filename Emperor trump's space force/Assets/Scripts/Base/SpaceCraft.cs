﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCraft : Hurtable_Char {

	[SerializeField]
    private string bulletId;
    [SerializeField]
    private float bulletSpeed;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected void Attack()
    {
        List<Transform> firePoints = new List<Transform>();
        foreach (Transform child in this.transform)
        {
            if(child.CompareTag("FirePoint"))
            {
               firePoints.Add(child);
            }
        }

        for (int i = 0; i < firePoints.Count; i++)
        {
           GameObject _temp = (GameObject)Instantiate(Resources.Load(bulletId), firePoints[i].transform.position, Quaternion.identity);
           _temp.GetComponent<Bullet_Base>().SetVariables(bulletSpeed, firePoints[i].transform.up);
        }
    }

}
