﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Base : MonoBehaviour {
    [SerializeField]
	private int bulletDamage;
    private float bulletSpeed;
	[SerializeField]
	private LayerMask targetLayer;
	[SerializeField]
	private float hitDistance;
	void Update () 
	{
		if(this.transform.position.x < Camera.main.transform.position.x -160 || transform.position.x > Camera.main.transform.position.x + 160 || transform.position.y < Camera.main.transform.position.y - 100 || transform.position.y  > Camera.main.transform.position.y + 100)
		{
          Destroy(this.gameObject);
		}

		transform.position += transform.up * Time.deltaTime *bulletSpeed;
		RaycastHit2D targetHit = Physics2D.Raycast(this.transform.position, transform.up, hitDistance, targetLayer);
		if(targetHit)
		{
			if(targetHit.transform.tag != "Indestructible" && !targetHit.transform.GetComponent<Hurtable_Char>().invulnerbale)
			{
               targetHit.transform.GetComponent<Hurtable_Char>().Hurt(bulletDamage);
			   Destroy(this.gameObject);
			}
        Destroy(this.gameObject);
		}

	}
	public void SetVariables(float bulletSpeed, Vector2 direction)
	{
     this.gameObject.transform.up = direction;
	 this.bulletSpeed = bulletSpeed;
	}
}
