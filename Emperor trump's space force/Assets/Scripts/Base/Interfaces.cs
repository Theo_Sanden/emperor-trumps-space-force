﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Icharacter
{
   void Movement();
   void Attack();
}
public interface Ihurtable<D>
{
    void Hurt(D damage);
}
public interface IDestroyable<H>
{
    void Destroy(H hitsTilDeath);
}
