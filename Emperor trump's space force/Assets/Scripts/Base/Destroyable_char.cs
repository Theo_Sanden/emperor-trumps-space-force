﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable_char : MonoBehaviour, IDestroyable<int> {

	protected int hitPoints;

	[SerializeField]
	int baseHitPoints;

	public int BaseHitPoints
	{
		get{ return BaseHitPoints;}
	}
	public int HitPoints
	{
		get{return hitPoints;}
	}

	
	void Awake () {
		hitPoints = baseHitPoints;
	}
	
	// Update is called once per frame
	void Update (){
	
	Destroy(HitPoints);
		
	}
    public void Destroy(int hitsTilDeath)
	{
		if(hitsTilDeath <= 0)
		{
           Destroy(this.gameObject);
		}
	}
}
