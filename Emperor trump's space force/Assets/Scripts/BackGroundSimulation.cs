﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundSimulation : Parallax {

	[SerializeField]
	int TileWidth;
	[SerializeField]
	int gridWidth;
	[SerializeField]
	GameObject tile;
	[SerializeField]
	Sprite tileSprite;
	List<GameObject> tilesCreated = new List<GameObject>();
	[SerializeField]
	Vector3 spawnBounderies;
	void Start () {

		for (int h = 0; h < 10; h++)
		{
			for (int b = 0; b < 10; b++)
			{
				GameObject temp_Save = Instantiate(tile, new Vector3(h,b,0) * (TileWidth*2), Quaternion.identity);

				temp_Save.GetComponent<SpriteRenderer>().sprite = tileSprite;

				temp_Save.transform.parent = this.gameObject.transform;

				temp_Save.transform.localPosition = new Vector3(h,b,0) * TileWidth*2;

			    tilesCreated.Add(temp_Save);

			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		/*int originalLength = tilesCreated.Count;

		for (int i = 0; i < originalLength; i++)
		{
			GameObject tile = tilesCreated[i];
			Vector3 newTileCoordinates = tile.transform.position * -1;
			Vector3 spawnBounderiesCamera = (Camera.main.transform.position + spawnBounderies);
			if(tile.transform.position.y > spawnBounderiesCamera.y || tile.transform.position.y < -spawnBounderies.y || tile.transform.position.x > spawnBounderies.x ||tile.transform.position.x < -spawnBounderiesCamera.x)
			{
               
			   tilesCreated.Add(Instantiate(this.tile, newTileCoordinates, Quaternion.identity));
			}
			
		}*/
		
	}
}
