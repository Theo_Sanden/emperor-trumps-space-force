﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip_Enemy_Basic : SpaceCraft {

    [SerializeField]
	private bool playerAggro;
	public bool PlayerAggro
	{
		get{return playerAggro;}
		set{playerAggro = value;}
	}
	[SerializeField]
	private int aggroRadiusPlayer;
	[SerializeField]
	private int aggroRadiusAssist;
    [SerializeField]
	private Vector2 movemenBounderies;
	private Vector2 startPosition;
	private bool returningToBase;
	[SerializeField]
	private float turnStep;
	[SerializeField]
	private float forwardSpeed;
	[SerializeField]
	private float distanceToKeep;


	[SerializeField]
	private float fireCooldown;
	[SerializeField]
	private float bulletBurstCount;
	private float fireTime;
	[SerializeField]
	private float timeBetweenBullets;
	private float bulletTimer;
	private float cooldownTimer;
	private bool cooldown = false;
	private float timerThreshold;
	[SerializeField]
	private LayerMask alliesLayermask;
	void Start () {
		startPosition = transform.position;
		fireTime = bulletBurstCount * timeBetweenBullets;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!playerAggro)
		{
          CheckPlayerAggro();
		}
		
		if(playerAggro)
		{
			timerThreshold = (!cooldown)? fireTime : fireCooldown;
			cooldownTimer = (cooldownTimer > timerThreshold)? 0 : cooldownTimer + Time.deltaTime;
			if(cooldownTimer == 0)
			{
              cooldown = !cooldown;
			  if(cooldown == false)
			  {
				  bulletTimer = 0;
			  }
			}
			if(cooldown == false && Vector3.Distance(this.gameObject.transform.position, GameObject.Find("Player").transform.position) <= aggroRadiusPlayer)
			{
               bulletTimer = (bulletTimer > timeBetweenBullets)? 0: bulletTimer + Time.deltaTime;
			   if(bulletTimer == 0)
			   {
                  Attack();

			   }
			}


            this.transform.up -= (this.transform.position - GameObject.Find("Player").transform.position) * 1/turnStep;
			if(Vector3.Distance(this.gameObject.transform.position, GameObject.FindGameObjectWithTag("Player").transform.position) > distanceToKeep)
			{
				transform.position += forwardSpeed * Time.deltaTime * transform.up;
			}
		}
		if(!playerAggro)
		{

		}
	}
	void CheckPlayerAggro()
	{
		Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		if(CalcAggroRange(playerPosition, aggroRadiusPlayer))
		{
			playerAggro = true;
			Collider2D[] alliesInArea = Physics2D.OverlapCircleAll(this.gameObject.transform.position, aggroRadiusAssist, alliesLayermask);
            Debug.Log(alliesInArea.Length);
			foreach (Collider2D collider in alliesInArea)
			{
				collider.gameObject.GetComponent<SpaceShip_Enemy_Basic>().PlayerAggro = true;
				Debug.Log(collider.gameObject.GetComponent<SpaceShip_Enemy_Basic>().name);
			}
		}
	}
	/*void CheckAssistAggro(GameObject[] friendlySpaceships)
	{
		foreach (GameObject spaceship in friendlySpaceships)
		{
            if(CalcAggroRange(spaceship.transform.position, aggroRadiusAssist) && spaceship.GetComponent<SpaceShip_Enemy_Basic>().PlayerAggro)
			{
				playerAggro = true;
			}	
			
		}	
	}*/
	bool CalcAggroRange(Vector2 targetPosition, float radius)
	{
        Vector2 aggroRadiusCenter = this.gameObject.transform.position;
		float d = Mathf.Sqrt(Mathf.Pow(targetPosition.x - aggroRadiusCenter.x,2) + Mathf.Pow(targetPosition.y - aggroRadiusCenter.y,2));
		if(d < radius)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
}
