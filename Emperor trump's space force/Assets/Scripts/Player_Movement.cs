﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : SpaceCraft {

	[SerializeField]
	float forwardsSpeed;

	[SerializeField]
	float turnSpeed;

	[SerializeField]
	Sprite[] movementSprites;

	private SpriteRenderer thisSpriterenderer;
	public KeyCode[] horizontalKeys = new KeyCode[]{KeyCode.A, KeyCode.D};
	private float bulletTimer;
    [SerializeField]
	float timeBetweenBullets;
	void Start () {
	thisSpriterenderer = gameObject.GetComponent<SpriteRenderer>();
	//this.transform.GetComponentInChildren<ShadowScript>().AttachShadow(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		

	bulletTimer -= Time.deltaTime;
	if(bulletTimer < 0)
	{
		bulletTimer = 0;
	}

	float directionForward = (Input.GetAxisRaw("Vertical") > 0)? Input.GetAxisRaw("Vertical") : 0;
	float directionRotation = (Input.GetKey(horizontalKeys[0]) == true && Input.GetKey(horizontalKeys[1]) == true)? 0: -Input.GetAxisRaw("Horizontal"); 
    transform.position += transform.up * directionForward * forwardsSpeed * Time.deltaTime;
	transform.Rotate(Vector3.forward * Time.deltaTime * turnSpeed * directionRotation);

	if(Input.GetAxisRaw("Fire1") > 0 && bulletTimer == 0)
	{
		bulletTimer = timeBetweenBullets;
		Attack();
	
	}
   


    CheckSpriteMovementTurbines();
	}
	void CheckSpriteMovementTurbines()
	{
          if(Input.GetAxisRaw("Vertical") == 1 && Input.GetAxisRaw("Horizontal") == 1)
		  {
             thisSpriterenderer.sprite = movementSprites[1];
		  }
		  else if(Input.GetAxisRaw("Vertical") == 1 && Input.GetAxisRaw("Horizontal") == -1)
		  {
             thisSpriterenderer.sprite = movementSprites[2];
		  }
		  else if(Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == 1)
		  {
             thisSpriterenderer.sprite = movementSprites[3];
		  }
		  else if(Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == -1)
		  {
             thisSpriterenderer.sprite = movementSprites[4];
		  }
		  else if(Input.GetAxisRaw("Vertical") == 1)
		  {
             thisSpriterenderer.sprite = movementSprites[5];
		  }
		  else if(Input.GetAxisRaw("Vertical") == 0)
		  {
             thisSpriterenderer.sprite = movementSprites[0];
		  }
	}
}
