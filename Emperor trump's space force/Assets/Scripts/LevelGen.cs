﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGen : MonoBehaviour {
    [SerializeField]
    Texture2D renderMap;
	[SerializeField]
	int tileSize;

	[SerializeField]
	Vector2 offset;
	[SerializeField]
    Sprite[] sprites;
	[SerializeField]
	GameObject tile;

 [SerializeField]
	private List<GameObject> createdTiles = new List<GameObject>();

	// Use this for initialization
	void Start () {
		RenderLevel();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void RenderLevel()
    {
        Color32[] pixels = renderMap.GetPixels32();
        int textWidth = renderMap.width;
		

        for (int i = 0; i < pixels.Length; i++)
        {
			
                Vector2 position = new Vector2(i % textWidth * tileSize, ((i - (i % textWidth)) / textWidth) * tileSize) + offset;
                if (pixels[i] == Color.black)
				{
					GameObject createdTile = Instantiate(tile, position, Quaternion.identity);
					SpriteRenderer createdTileSR = createdTile.GetComponent<SpriteRenderer>();

					if(pixels[i+1] == Color.black && pixels[i-1] == Color.white && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.white)
					{
                        createdTileSR.sprite = sprites[0];
					}
					else if(pixels[i+1] == Color.black && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.white)
					{
                        createdTileSR.sprite = sprites[1];
					}
					else if(pixels[i+1] == Color.white && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.white)
					{
                        createdTileSR.sprite = sprites[2];
					}
					else if(pixels[i+1] == Color.black && pixels[i-1] == Color.white && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[3];
					}
					else if(pixels[i+1] == Color.black && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[4];
					}
					else if(pixels[i+1] == Color.white && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.black && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[5];
					}
					else if(pixels[i+1] == Color.black && pixels[i-1] == Color.white && pixels[i-textWidth] == Color.white && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[6];
					}
					else if(pixels[i+1] == Color.black && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.white && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[7];
					}
					else if(pixels[i+1] == Color.white && pixels[i-1] == Color.black && pixels[i-textWidth] == Color.white && pixels[i+textWidth] == Color.black)
					{
                        createdTileSR.sprite = sprites[8];
					}
                    createdTile.transform.SetParent(GameObject.Find("Foreground").transform);
                   createdTiles.Add(createdTile);
                {
		}
    }
}
	}
}

	
