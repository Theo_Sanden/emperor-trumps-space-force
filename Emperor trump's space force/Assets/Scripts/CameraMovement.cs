﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	GameObject AttachableObject;
	[SerializeField]
	Vector3 offsetCenterPlayer;
	void Start () {
		AttachableObject = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = new Vector3(AttachableObject.transform.position.x, AttachableObject.transform.position.y, this.transform.position.z) + offsetCenterPlayer;
	}
}
