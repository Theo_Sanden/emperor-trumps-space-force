﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	float speed;

	Vector3 cameraTransformLastFrame;
	void Start () {
		cameraTransformLastFrame = Camera.main.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
        
		Vector3 cameraVelocity =  cameraTransformLastFrame - Camera.main.transform.position;


		transform.position += new Vector3((cameraVelocity.x * speed),(cameraVelocity.y * speed),0);


		cameraTransformLastFrame = Camera.main.transform.position;
	}
}
